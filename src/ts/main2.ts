interface Question {
  ption: string;
  choices: string[];
  correctAnswer: number;
}

const ptions: Question[] = [
  {
    ption:
      "What is the output of the following code?\n\nconsole.log(typeof null);",
    choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
    correctAnswer: 0,
  },
  {
    ption:
      "Which method is used to add one or more elements to the end of an array?",
    choices: ["push()", "join()", "slice()", "concat()"],
    correctAnswer: 0,
  },
  {
    ption: 'What is the result of the following expression?\n\n3 + 2 + "7"',
    choices: ['"327"', '"12"', '"57"', '"NaN"'],
    correctAnswer: 2,
  },
  {
    ption: 'What is the purpose of the "use strict" directive in JavaScript?',
    choices: [
      "Enforce stricter type checking",
      "Enable the use of modern syntax",
      "Enable strict mode for improved error handling",
      "Disable certain features for better performance",
    ],
    correctAnswer: 2,
  },
  {
    ption: 'What is the scope of a variable declared with the "let" keyword?',
    choices: ["Function scope", "Global scope", "Block scope", "Module scope"],
    correctAnswer: 2,
  },
  {
    ption:
      "Which higher-order function is used to transform elements of an array into a single value?",
    choices: ["map()", "filter()", "reduce()", "forEach()"],
    correctAnswer: 2,
  },
  {
    ption: 'What does the "=== " operator in JavaScript check for?',
    choices: [
      "Equality of values",
      "Equality of values and types",
      "Inequality of values",
      "Reference equality",
    ],
    correctAnswer: 1,
  },
  {
    ption: 'What is the purpose of the "this" keyword in JavaScript?',
    choices: [
      "Refer to the current function",
      "Refer to the parent function",
      "Refer to the global object",
      "Refer to the object that owns the current code",
    ],
    correctAnswer: 3,
  },
  {
    ption: 'What does the "NaN" value represent in JavaScript?',
    choices: ["Not a Number", "Null", "Negative Number", "Not Applicable"],
    correctAnswer: 0,
  },
  {
    ption: "Which method is used to remove the last element from an array?",
    choices: ["pop()", "shift()", "slice()", "splice()"],
    correctAnswer: 0,
  },
];
const h = document.getElementById("ex2") as HTMLDivElement;
const divv = document.createElement("div");
const score = document.createElement("div");
let Score = 0;
score.innerText = "Current Score: " + Score + "/10";
h.appendChild(score);
h.appendChild(divv);

for (let q = 0; q < ptions.length; q++) {
  const ption = ptions[q];
  const p = document.createElement("p");
  p.innerText = ption.ption;
  divv.appendChild(p);
  const NumAns: HTMLInputElement[] = [];
  let ans: number | null;

  for (let c = 0; c < ption.choices.length; c++) {
    const Ans = c;
    const choice = document.createElement("div");
    const ch = document.createElement("input");
    ch.type = "radio";
    const span = document.createElement("span");
    span.innerText = ption.choices[c];
    ch.addEventListener("change", () => {
      for (const n of NumAns) n.checked = !1;
      (ch.checked = !0), (ans = Ans);
    }),
      NumAns.push(ch);
    choice.appendChild(ch);
    choice.appendChild(span);
    divv.appendChild(choice);
  }

  const bt = document.createElement("button");
  bt.innerText = "Submit";
  bt.addEventListener("click", function () {
    if (ans == null) {
      alert("please choose an answer first!");
      return;
    }
    if (ans == ption.correctAnswer) {
      result.innerText = "Correct!";
      Score++;
      score.innerText = "Current Score: " + Score + "/10";
      for (const n of NumAns) {
        n.disabled = !0;
        bt.disabled = !0;
      }
    } else {
      result.innerText = "Incorrect!";
      for (const n of NumAns) {
        n.disabled = !0;
        bt.disabled = !0;
      }
    }
  });

  divv.appendChild(bt);
  const result = document.createElement("p");
  divv.appendChild(result);
}
